//////////////////////////////////////////////////////
//  DroneSpeechROSModule.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef DRONE_SPEECH_ROS_MODULE_H
#define DRONE_SPEECH_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"


// Message
#include <sound_play/SoundRequest.h>
#include <std_msgs/String.h>


/////////////////////////////////////////
// Class DroneSpeechROSModule
//
//   Description

// Voices
// /usr/share/festival
// sudo apt-get install festlex-cmu
// http://ubuntuforums.org/showthread.php?t=751169


//
/////////////////////////////////////////
class DroneSpeechROSModule : public DroneModule
{
protected:
    std::string nodeName;


protected:
    std::string voice;


protected:
    std::string messageToSayTopicName;
    ros::Subscriber messageToSaySub;
    void messageToSayCallback(const std_msgs::String::ConstPtr &msg);



protected:
    std::string soundTopicName;
    ros::Publisher soundPub;
    sound_play::SoundRequest soundMsg;
public:
    int soundPublish();


public:
    DroneSpeechROSModule();
    ~DroneSpeechROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

};




#endif
